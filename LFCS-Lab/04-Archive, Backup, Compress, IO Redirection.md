# Archive, Backup, Compress, IO Redirection 

**Question 1**

Create a tar archive logs.tar (under bob's home) of /var/log/ directory.

<details><summary>Solution</summary>

<br>

- `sudo tar cvfP logs.tar /var/log`

</details>

**Question 2**

Create a compressed tar archive logs.tar.gz (under bob's home) of /var/log/ directory.

<details><summary>Solution</summary>

<br>

- `sudo tar czfP logs.tar.gz /var/log`

</details>

**Question 3**

List the content of /home/bob/logs.tar archive and save the output in /home/bob/tar_data.txt file.

<details><summary>Solution</summary>

<br>

- `sudo tar -tf logs.tar > /home/bob/tar_data.txt`

</details>

**Question 4**

Extract the contents of /home/bob/archive.tar.gz to the /tmp directory.

<details><summary>Solution</summary>

<br>

- `sudo tar xvfP /home/bob/archive.tar.gz -C /tmp`

</details>

**Question 5**

Execute /home/bob/script.sh script and save all normal output (except errors/warnings) in /home/bob/output_stdout.txt file.

<details><summary>Solution</summary>

<br>

- `sudo ./script.sh > /home/bob/output_stdout.txt`

</details>

**Question 6**

Execute /home/bob/script.sh script and save all command output (both errors/warnings and normal output) in /home/bob/output.txt file.

<details><summary>Solution</summary>

<br>

- `sudo ./script.sh > /home/bob/output.txt 2>&1`

</details>

**Question 7**

Execute /home/bob/script.sh script and save all errors only in /home/bob/output_errors.txt file.

<details><summary>Solution</summary>

<br>

- `sudo ./script.sh 2> /home/bob/output_errors.txt`

</details>

**Question 8**

Create a bzip archive under bob's home named file.txt.bz2 out of /home/bob/file.txt, but preserve the original file. At the end of the exercise you should have both:

<details><summary>Solution</summary>

<br>

- `sudo bzip2 -k file.txt.bz2 /home/bob/file.txt`

</details>

**Question 9**

Extract the contents of /home/bob/archive.tar.gz to the /opt directory.

<details><summary>Solution</summary>

<br>

- `sudo tar xvf /home/bob/archive.tar.gz -C /opt`

</details>

**Question 10**

Use the cat command, and redirection, to add (append) the contents of /home/bob/file.txt to /home/bob/destination.txt.

<details><summary>Solution</summary>

<br>

- `cat file.txt >> destination.txt`

</details>

**Question 11**

Create a file.tar archive of /home/bob/file directory under /home/bob location.

<details><summary>Solution</summary>

<br>

- `tar cvfP file.tar file`

</details>

**Question 12**

Create gzip archive of games.txt file , which is present under /home/bob directory.

<details><summary>Solution</summary>

<br>

- `gzip games.txt`

</details>

**Question 13**

We have a /home/bob/lfcs.txt.xz file, uncompress it under /home/bob/.

<details><summary>Solution</summary>

<br>

- `xz -d /home/bob/lfcs.txt.xz`

</details>

**Question 14**

Sort the contents of /home/bob/values.conf file alphabetically and eliminate any common values, save the sorted output in /home/bob/values.sort file.

<details><summary>Solution</summary>

<br>

- `sort /home/bob/values.conf | uniq > /home/bob/values.sort`

</details>

**Question 15**

Sort again the contents of /home/bob/values.conf file alphabetically, eliminate any common values and ignore case.
Finally save the sorted output in /home/bob/values.sorted file.

<details><summary>Solution</summary>

<br>

- `sort /home/bob/values.conf | uniq -i > /home/bob/values.sorted`

</details>
