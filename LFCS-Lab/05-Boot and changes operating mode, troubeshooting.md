# Boot and changes operating mode, troubeshooting bootloader

**Question 1**

On a system booting through legacy BIOS mode, what file would you edit before generating a new grub configuration file?

<details><summary>Solution</summary>

<br>

- `/etc/default/grub`

</details>

**Question 2**

After you edited the file mentioned previously, what command would you use to generate the new grub configuration file?

<details><summary>Solution</summary>

<br>

- `sudo grub2-mkconfig -o /etc/grub2.cfg` Maybe NOT

</details>

**Question 3**

Schedule this system to power off two hours later from now.

<details><summary>Solution</summary>

<br>

- `sudo shutdown +120`

</details>

**Question 4**

The system is currently booting to a text-only console.
Change it to boot to a graphical desktop by default.

<details><summary>Solution</summary>

<br>

- `sudo systemctl set-default graphical.target`

</details>

**Question 5**

Change grub's default timeout from 1 seconds to 5 second.

<details><summary>Solution</summary>

<br>

- `sudo vi /etc/default/grub`

</details>

**Question 6**

Install grub to /dev/vda.

Make sure to save the installation command output to /home/bob/grub.txt file.

<details><summary>Solution</summary>

<br>

- `sudo grub2-install /dev/vda > /home/bob/grub.txt 2>&1`

</details>

**Question 7**

Find out what is the system's current default boot target and save the value in /home/bob/boot-target.txt file.

<details><summary>Solution</summary>

<br>

- `sudo systemctl get-default > /home/bob/boot-target.txt`

</details>

**Question 8**

Cancel the scheduled shutdown you configured in the beginning.

<details><summary>Solution</summary>

<br>

- `sudo shutdown -c`

</details>
