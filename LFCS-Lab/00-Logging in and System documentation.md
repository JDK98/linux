# Logging in and System documentation


**Question 1**

The **ssh** command has an option to display its version number.
Use **man** to find out what is the correct command line option.

<details><summary>Solution</summary>

<br>

- `ssh -V`

</details>


**Question 2**

Find out using which command you can change the static hostname of your Linux system?

<details><summary>Solution</summary>

<br>

- `hostnamectl`

</details>

**Question 3**

 If the apropos command does not work because your manual pages are not indexed, what command you can use to manually refresh these?

<details><summary>Solution</summary>

<br>

- `mandb`

</details>


**Question 4**

 You are trying to use **ssh alex@localhost** to log in through SSH. Your connection is refused. ssh has a command line option to show you the verbose output. That will show a lot more status messages and help you debug why this connection is failing. What is the correct option for that? (you need not to make ssh connection work at this point)

<details><summary>Solution</summary>

<br>

- `ssh -v alex@localhost`

</details>

**Question 5**

 How many hidden files are there in **/home/bob/data/** directory?

<details><summary>Solution</summary>

<br>

- `ls -a data/`

</details>

**Question 6**

 SSH into **dev-host01** host from centos-host and create a blank file called **/home/bob/myfile** in dev-host01 host.
You should be able to create the file using touch /home/bob/myfile command. <br>
Please find below the SSH credentials for **dev-host01** host:

```
 Host: dev-host01
 Username: bob
 Password: caleston123
```
<details><summary>Solution</summary>

<br>

- `ssh bob@dev-host01`

- `touch myfile`

</details>

**Question 7**

 We are trying to run apropos ssh command to get some details about the commands related to ssh but we are getting this error:

`ssh: nothing appropriate.`

 Look into the issue and fix the same to make apropos ssh command work.

<details><summary>Solution</summary>

<br>

- `sudo mandb`

- `apropos ssh`

</details>

**Question 8**

 Using apropos command, find out the configuration file for NFS mounts and save its name in /home/bob/nfs file.

<details><summary>Solution</summary>

<br>

- `apropos nfs | grep -i "NFS mounts `

- `echo "--Result--" >> nfs`

</details>

