# Scripting, manage startup process

**Question 1**

How do we run script.sh that is located in our current directory?

<details><summary>Solution</summary>

<br>

- `./script.sh`

</details>

**Question 2**

What is the correct shebang to add in a script?

<details><summary>Solution</summary>

<br>

- `#!/bin/bash`

</details>

**Question 3**

Make sure rpcbind.service unit automatically starts up after Linux boots.

Note: Do not try to reboot the machine for testing.

<details><summary>Solution</summary>

<br>

- `sudo systemctl enable rpcbind.service`

</details>

**Question 4**

Under bob's home:

Create a script called script.sh. This script should create a tar archive called archive.tar.gz. The script should archive the directory called dir1.

Please make sure you execute the script at least once.

<details><summary>Solution</summary>

<br>

- `vi script.sh`

```
#!/bin/bash
tar czvf archive.tar.gz dir1
```

- `chmod u+x script.sh`

</details>

**Question 5**

There is a service unit that automatically starts up the SSH daemon. Use the correct command to find out the PID assigned to the process launched by this service.

Save the PID in /home/bob/pid file.

<details><summary>Solution</summary>

<br>

- `ps aux | grep ssh`

</details>

**Question 6**

Under bob's home:
Create script2.sh script that displays if the sshd.service unit is enabled or disabled.

Remember to make this script executable and try to execute it at least once to verify your answer.

<details><summary>Solution</summary>

<br>

 ```
 #!/bin/bash
 sudo systemctl is-enabled sshd.service
 ```

</details>

**Question 7**

Create a script /home/bob/perm.sh. This script should set permissions on /home/bob/dir8 directory so that user owner only has x (execute) permissions, group owner and others must not have any permissions at all.

Remember to make this script executable and try to execute it at least once to verify your answer.

<details><summary>Solution</summary>

<br>

 ```
 #!/bin/bash
 chmod 100 /home/bob/dir8
 ```

</details>

**Question 8**

httpd is already installed, mask its service.

<details><summary>Solution</summary>

<br>

- `sudo systemctl mask httpd`

</details>

**Question 9**

We made some changes on httpd unit file i.e /usr/lib/systemd/system/httpd.service and after that we are getting some warnings while running systemctl status httpd command.
Fix the same and make sure this command works without any warning.

<details><summary>Solution</summary>

<br>

- `sudo systemctl daemon-reload`

</details>
