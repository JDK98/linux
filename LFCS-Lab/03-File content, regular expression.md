# File content, regular expression 

**Question 1**

Which of the following commands can be used to manipulate strings in a file?

<details><summary>Solution</summary>

<br>

- `sed`

</details>

**Question 2**

Which of the following commands you will use to display the top 5 lines of a file called myfile?

<details><summary>Solution</summary>

<br>

- `head -5 myfile`

</details>

**Question 3**

Which of the following commands you can use to filter out the lines that contain a particular pattern?

<details><summary>Solution</summary>

<br>

- `grep`

</details>

**Question 4**

How can we ignore the case (small or capital) differences while comparing two files using diff command?

<details><summary>Solution</summary>

<br>

- `diff -i`

</details>

**Question 5**

You have below given content in /home/bob/testfile (this is just an example file):

a;b;c;d
x;y;z

How would you extract/print the b and the y?

<details><summary>Solution</summary>

<br>

- `cut -d ';' -f 2 testfile`

</details>

**Question 6**

Change all values enabled to disabled in /home/bob/values.conf config file.

<details><summary>Solution</summary>

<br>

- `sudo sed -i 's/enabled/disabled/g' /home/bob/values.conf`

</details>

**Question 7**

While ignoring the case sensitivity, change all values disabled to enabled in /home/bob/values.conf config file.

For example, any string like disabled, DISABLED, Disabled etc must match and should be replaced with enabled.

<details><summary>Solution</summary>

<br>

- `sudo sed -i "s/disabled/enabled/gi" /home/bob/values.conf`

</details>

**Question 8**

Change all values enabled to disabled in /home/bob/values.conf config file from line number 500 to 2000.

<details><summary>Solution</summary>

<br>

- `sudo sed -i '500,2000s/enabled/disabled/g' /home/bob/values.conf`

</details>

**Question 9**

Replace all occurrence of string #%$2jh//238720//31223 with $2//23872031223 in /home/bob/data.txt file.

<details><summary>Solution</summary>

<br>

- `sudo sed -i 's~#%$2jh//238720//31223~$2//23872031223~g' /home/bob/data.txt`

</details>

**Question 10**

Filter out the lines that contain any word that starts with a capital letter and are then followed by exactly two lowercase letters in /etc/nsswitch.conf file and save the output in /home/bob/filtered1 file.

You can use the redirection to save your command's output in a file i.e [your-command] > /home/bob/filtered1

<details><summary>Solution</summary>

<br>

- `egrep -w "[A-Z][a-z]{2}" /etc/nsswitch.conf > /home/bob/filtered1`

</details>

**Question 11**

Open /home/bob/testfile file in any editor (vi, nano etc) and move line present on line no:1049 to line no: 5.

<details><summary>Solution</summary>

<br>

- `vi testfile`
- In Command Mode -> `:1049``
- In Iommand mode press -> `dd`
- In Command Mode press -> `:5`
- In Command Mode press -> `pp` 

</details>

**Question 12**

Delete first 1000 lines from /home/bob/testfile file.

<details><summary>Solution</summary>

<br>

- `vi testfile`
- In Command Mode -> `1000` then `dd`
 

</details>

**Question 13**

/home/bob/file1 and /home/bob/file2 are 99% identical. But there's 1 unique line that exists only in /home/bob/file1 or in /home/bob/file2.

Find that line and save the same in /home/bob/file3 file.

<details><summary>Solution</summary>

<br>

- `diff /home/bob/file1 /home/bob/file2 > /home/bob/file3`
 
</details>

**Question 14**

In /home/bob/textfile file there's a number that has 5 digits. Save the same in /home/bob/number file.

You can use the redirection to save your command's output in a file i.e [your-command] > /home/bob/number

<details><summary>Solution</summary>

<br>

- `egrep '[0-9]{5}' /home/bob/textfile > /home/bob/number`
 
</details>

**Question 15**

How many numbers in /home/bob/textfile begin with a number 2, save the count in /home/bob/count file.

You can use the redirection to save your command's output in a file i.e [your-command] > /home/bob/count

<details><summary>Solution</summary>

<br>

- `egrep -c '^2' /home/bob/textfile > /home/bob/count`
 
</details>

**Question 16**

How many lines in /home/bob/testfile file begin with string Section, regardless of case.
Save the count in /home/bob/count_lines file.

<details><summary>Solution</summary>

<br>

- `egrep -i -c '^Section' /home/bob/testfile > /home/bob/count_lines`
 
</details>

**Question 17**

Find all lines in /home/bob/testfile file that contain string man, it must be an exact match.
For example the line like # before /usr/man. or NOCACHE keeps man should match but # given manpath. or For a manpath must not match.

Save the filtered lines in /home/bob/man_filtered file.

<details><summary>Solution</summary>

<br>

- `grep -w 'man' /home/bob/testfile > /home/bob/man_filtered`
 
</details>

**Question 18**

Save last 500 lines of /home/bob/textfile file in /home/bob/last file.

<details><summary>Solution</summary>

<br>

- `tail -500 /home/bob/textfile > last`
 
</details>
